#!/usr/bin/env python

import subprocess
import requests
import argparse
import os
import json
import socket

SSH=os.path.expanduser("~/.ssh/id_rsa.pub")
HOST_NAME='vsc_' + socket.gethostname()

def generateSSH(email, service_name):
    #TODO: add checks later
    if os.path.isfile(SSH):
        print("Reusing file " + SSH)
        return
    print("Generating file " + SSH)
    subprocess.call(["ssh-keygen",
                     "-t", "rsa",
                     "-b", "4096",
                     "-C", email,
                     "-N", ""])


def err(res):
    msg = json.loads(res.text)
    msg_str = json.dumps(msg, indent=4)
    print("[ERR] %s: %s" % (res.status_code, msg_str))

def github(token):
    key = open(SSH, "r").read()
    headers = {
        "Authorization": "token " + token,
        "X-OAuth-Scopes" : "repo, user",
        "X-Accepted-OAuth-Scopes": "admin:public_key, user",
        "Accept": "application/json"
    }
    r = requests.post('https://api.github.com/user/keys',
                      headers=headers,
                      data=json.dumps({
                          'key': key,
                          'title': HOST_NAME
                      }))
    
    if (r.status_code != 201):
        err(r)
        return

def gitlab(token):
    key = open(SSH, "r").read()
    headers = {
        'PRIVATE-TOKEN': token
    }
    r = requests.post('https://gitlab.com/api/v3/user/keys',
                      headers=headers,
                      data={
                          'key': key,
                          'title': HOST_NAME
                      })
    
    if (r.status_code != 201):
        err(r)
        return


#TODO: add server to store git config for various applications that your have
def main():
    parser = argparse.ArgumentParser(description="VSC Initializer 2016 (c) Simeon Mugisha")
    parser.add_argument('--token', help="access token for the vsc service")
    parser.add_argument('service', help="name of service")
    parser.add_argument('--email', help="user email", required=True)
    parser.add_argument('--name', help="full name")
    args = parser.parse_args()

    token = args.token
    if token == None:
        token = os.environ.get(args.service.upper() + '_TOKEN')
        if token == None:
            print("[ERR] No token provided")
            return
    print(token)
    generateSSH(args.email, args.service)

    if args.service == "github":
        github(token)
    elif args.service == "gitlab":
        gitlab(token)


if __name__ == "__main__":
    main()
